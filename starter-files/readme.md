# Brainster Weekly Challenge 1 - Pagination

## Info/Sending:

1. Place the challenge in a this GitLab project.
   1. Each group in a separate branch (DO NOT WORK ON MASTER).
   2. Name your branch - **weekly-challenge-group-number**
   3. You will get the solution afterwards on master.
2. You have 1.5/2 hours to divide your work & finish this challenge.

## Tasks:

1. Make the HTML/CSS accordingly based on the screenshot provided.
2. db.js contains the info about the cars that should be shown.
3. Make the js-logic:
   1. Each page is showing 5 cards per page and no more (first page (1-5 cards), second page(6-10 cards) etc.).
   2. Render the first page of cards.
   3. Based on all cards, find out how many links there should be in the pagination and render them on screen. (do not make them static in html).
   4. Make the trigger on each of the pages work. (clicking on each page in the pagination, adds active class to it as shown in the screenshot on the first page)
   5. Make the trigger on each of the arrows work.
   6. *each of these triggers should render the cards on the same page (not in another html page)*

## Bonus:

1. Scroll to the top of the page each time you render a new page on the screen.