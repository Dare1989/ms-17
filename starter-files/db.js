const cars = [
  {
    id: 1,
    car_model: 'Toyota',
    car_desc:
      'Sed sagittis. Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci. Nullam molestie nibh in lectus. Pellentesque at nulla. Suspendisse potenti.',
    car_year: 2007,
    car_img: 'http://dummyimage.com/350x250.png/dddddd/000000'
  },
  {
    id: 2,
    car_model: 'Subaru',
    car_desc:
      'Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque.',
    car_year: 2002,
    car_img: 'http://dummyimage.com/350x250.png/dddddd/000000'
  },
  {
    id: 3,
    car_model: 'Dodge',
    car_desc:
      'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis faucibus accumsan odio. Curabitur convallis. Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus.',
    car_year: 1994,
    car_img: 'http://dummyimage.com/350x250.png/5fa2dd/ffffff'
  },
  {
    id: 4,
    car_model: 'Saab',
    car_desc:
      'Cras in purus eu magna vulputate luctus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vivamus vestibulum sagittis sapien. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.',
    car_year: 2011,
    car_img: 'http://dummyimage.com/350x250.png/cc0000/ffffff'
  },
  {
    id: 5,
    car_model: 'Chrysler',
    car_desc:
      'In congue. Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst. Etiam faucibus cursus urna.',
    car_year: 2003,
    car_img: 'http://dummyimage.com/350x250.png/dddddd/000000'
  },
  {
    id: 6,
    car_model: 'Toyota',
    car_desc:
      'Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Donec pharetra, magna vestibulum aliquet ultrices, erat tortor sollicitudin mi, sit amet lobortis sapien sapien non mi. Integer ac neque. Duis bibendum.',
    car_year: 2000,
    car_img: 'http://dummyimage.com/350x250.png/cc0000/ffffff'
  },
  {
    id: 7,
    car_model: 'Jaguar',
    car_desc:
      'Maecenas ut massa quis augue luctus tincidunt. Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh.',
    car_year: 2010,
    car_img: 'http://dummyimage.com/350x250.png/cc0000/ffffff'
  },
  {
    id: 8,
    car_model: 'Chevrolet',
    car_desc:
      'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Proin risus. Praesent lectus. Vestibulum quam sapien, varius ut, blandit non, interdum in, ante.',
    car_year: 2007,
    car_img: 'http://dummyimage.com/350x250.png/5fa2dd/ffffff'
  },
  {
    id: 9,
    car_model: 'Jaguar',
    car_desc:
      'Nulla neque libero, convallis eget, eleifend luctus, ultricies eu, nibh. Quisque id justo sit amet sapien dignissim vestibulum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla dapibus dolor vel est. Donec odio justo, sollicitudin ut, suscipit a, feugiat et, eros. Vestibulum ac est lacinia nisi venenatis tristique.',
    car_year: 2010,
    car_img: 'http://dummyimage.com/350x250.png/cc0000/ffffff'
  },
  {
    id: 10,
    car_model: 'Mercedes-Benz',
    car_desc:
      'Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante. Nulla justo. Aliquam quis turpis eget elit sodales scelerisque.',
    car_year: 1989,
    car_img: 'http://dummyimage.com/350x250.png/5fa2dd/ffffff'
  },
  {
    id: 11,
    car_model: 'GMC',
    car_desc:
      'Fusce lacus purus, aliquet at, feugiat non, pretium quis, lectus. Suspendisse potenti. In eleifend quam a odio. In hac habitasse platea dictumst. Maecenas ut massa quis augue luctus tincidunt.',
    car_year: 2009,
    car_img: 'http://dummyimage.com/350x250.png/5fa2dd/ffffff'
  },
  {
    id: 12,
    car_model: 'Mitsubishi',
    car_desc:
      'Etiam justo. Etiam pretium iaculis justo. In hac habitasse platea dictumst.',
    car_year: 1995,
    car_img: 'http://dummyimage.com/350x250.png/ff4444/ffffff'
  },
  {
    id: 13,
    car_model: 'Volvo',
    car_desc:
      'Nam dui. Proin leo odio, porttitor id, consequat in, consequat ut, nulla. Sed accumsan felis.',
    car_year: 2008,
    car_img: 'http://dummyimage.com/350x250.png/ff4444/ffffff'
  },
  {
    id: 14,
    car_model: 'Chevrolet',
    car_desc:
      'Duis at velit eu est congue elementum. In hac habitasse platea dictumst. Morbi vestibulum, velit id pretium iaculis, diam erat fermentum justo, nec condimentum neque sapien placerat ante.',
    car_year: 2010,
    car_img: 'http://dummyimage.com/350x250.png/5fa2dd/ffffff'
  },
  {
    id: 15,
    car_model: 'Chevrolet',
    car_desc:
      'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa. Donec dapibus. Duis at velit eu est congue elementum.',
    car_year: 2005,
    car_img: 'http://dummyimage.com/350x250.png/ff4444/ffffff'
  },
  {
    id: 16,
    car_model: 'Mitsubishi',
    car_desc:
      'Donec vitae nisi. Nam ultrices, libero non mattis pulvinar, nulla pede ullamcorper augue, a suscipit nulla elit ac nulla. Sed vel enim sit amet nunc viverra dapibus. Nulla suscipit ligula in lacus.',
    car_year: 1989,
    car_img: 'http://dummyimage.com/350x250.png/5fa2dd/ffffff'
  },
  {
    id: 17,
    car_model: 'Ford',
    car_desc:
      'Proin at turpis a pede posuere nonummy. Integer non velit. Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue.',
    car_year: 1995,
    car_img: 'http://dummyimage.com/350x250.png/5fa2dd/ffffff'
  },
  {
    id: 18,
    car_model: 'Nissan',
    car_desc:
      'Morbi porttitor lorem id ligula. Suspendisse ornare consequat lectus. In est risus, auctor sed, tristique in, tempus sit amet, sem. Fusce consequat.',
    car_year: 1998,
    car_img: 'http://dummyimage.com/350x250.png/ff4444/ffffff'
  },
  {
    id: 19,
    car_model: 'Pontiac',
    car_desc:
      'Aliquam quis turpis eget elit sodales scelerisque. Mauris sit amet eros. Suspendisse accumsan tortor quis turpis.',
    car_year: 1969,
    car_img: 'http://dummyimage.com/350x250.png/ff4444/ffffff'
  },
  {
    id: 20,
    car_model: 'Toyota',
    car_desc:
      'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci.',
    car_year: 2002,
    car_img: 'http://dummyimage.com/350x250.png/dddddd/000000'
  },{
    id: 21,
    car_model: 'Toyota',
    car_desc:
      'Duis consequat dui nec nisi volutpat eleifend. Donec ut dolor. Morbi vel lectus in quam fringilla rhoncus. Mauris enim leo, rhoncus sed, vestibulum sit amet, cursus id, turpis. Integer aliquet, massa id lobortis convallis, tortor risus dapibus augue, vel accumsan tellus nisi eu orci.',
    car_year: 2002,
    car_img: 'http://dummyimage.com/350x250.png/dddddd/000000'
  }
];
